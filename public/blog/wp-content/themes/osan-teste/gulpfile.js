var gulp = require('gulp');
var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer')
var imagemin = require('gulp-imagemin');
sass.compiler = require('node-sass');
 

// Tarefa Sass, Minificar e renomear e prefixar o código pra todos os navegadores
gulp.task('sass', async function () {
    gulp.src('./assets/sass/**/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(autoprefixer({ cascade: false }))
    .pipe(cssmin())
    .pipe(rename('style.min.css'))
    .pipe(gulp.dest('./assets/css'));
});


// Minificar imagens
gulp.task('imagemin' , function(){
    gulp.src('./assets/images/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./assets/images/'))
});

// Tarefa Watch
gulp.task('watch', function(){
  gulp.watch('./assets/sass/**/*.scss', gulp.series('sass'));
});



// Tarefa Padrão
gulp.task('default', gulp.series('sass', 'watch'));



