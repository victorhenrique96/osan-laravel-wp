<?php

function carrega_scripts(){
    wp_enqueue_style('fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css');
	wp_enqueue_style('boostrap','https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css', array(),'','all');
	wp_enqueue_style('reset', get_template_directory_uri().'/assets/css/reset.css', array(),'1.0','all');
    wp_enqueue_style('style', get_template_directory_uri().'/assets/css/style.min.css', array(),'1.0','all');
    wp_enqueue_style('material', 'https://fonts.googleapis.com/icon?family=Material+Icons', array(),'1.0','all');

	
    wp_enqueue_script('jquery','https://code.jquery.com/jquery-3.3.1.slim.min.js', array(),null,true);
    wp_enqueue_script('main', get_template_directory_uri().'/assets/js/script.js', array(),null,true);
    wp_enqueue_script('popper','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array(),null,true);
    wp_enqueue_script('bootstrap','https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'),null,true);}

add_action('wp_enqueue_scripts','carrega_scripts');

// Definir as miniaturas
add_theme_support('post-thumbnails');
set_post_thumbnail_size (1280 , 720, true);

// Resumo da página
function add_support_to_pages() {
    add_post_type_support( 'page', 'excerpt' );
  }
  add_action( 'init', 'add_support_to_pages' );

//Definir tamanho do resumo
add_filter('excerpt_length', function($length){
    return 20;
} );

// Editor padrão
add_filter('use_block_editor_for_post', '__return_false', 10);

// Trocar o logo padrão do Wordpress
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/admin-logo.png);
            background-size: cover;
            width: 15rem;
            background-repeat: no-repeat;
            padding-bottom: 2rem;
            height: 7rem;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

//Função que altera a URL, trocando pelo endereço do seu site*/
function my_login_logo_url() {
    return get_bloginfo( 'url' );
    }
    add_filter( 'login_headerurl', 'my_login_logo_url' );
     
//Função que adiciona o nome do seu site, no momento que o mouse passa por cima da logo*/
function my_login_logo_url_title() {
    return 'Novidá Tecnologia - Voltar para Home';
    }
    add_filter( 'login_headertitle', 'my_login_logo_url_title' );

?>