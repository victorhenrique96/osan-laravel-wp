<?php get_header(); ?>

<section>
    <div class="container-fluid background-noticias">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <small class="caminho">Você está em: Home /<span class="text-info"> Notícias</span></small>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <p class="titulo">Notícias</p>
                        <div class="w-100 mt-5"></div>
                    </div>
                    <div class="col-lg-5">
                        <div class="searchbar">
                            <input class="search_input" type="text" name="">
                            <a href="#" class="search_icon"><i class="fas fa-search" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <div class="container noticias-box">
            <div class="container new-noticias">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <div class="card">
                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', array('class' => 'img-fluid')); ?></a>
                            <div class="card-body">
                            <small class="date-post"><?php the_time('F j, Y') ?></small>
                                <a class="card-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                <a class="card-text" href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
                                <button class="btn btn-outline-dark"><a class="post-link" href="<?php the_permalink(); ?>">+ Ler mais</a></button>
                            </div>
                        </div>

                    <?php endwhile; ?>
                <?php else : get_404_template();
                endif; ?>
            </div>
        </div>

        <div class="container">
            <div class="pagination">
                <?php the_posts_pagination(); ?>
            </div>
        </div>


        <?php get_footer(); ?>