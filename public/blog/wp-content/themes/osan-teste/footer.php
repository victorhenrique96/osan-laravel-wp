<!-- FOOTER DESKTOP -->
<div class="d-none d-lg-block">
<?php $home = get_template_directory_uri(); ?>
<?php $home_url = "http://ambiente-desenvolvimento.provisorio.ws/Victor/osan" ?>
    <div class="container-fluid border-top">
        <div class="row">
        <div class="w-100 mt-3"></div>
            <div class="col-lg-4">
            </div>
            <div class="col-lg">
                <p>Acesso rápido</p>
            </div>
        </div>
<div class="container">
    <div class="row">
            <div class="col-lg-2">
                <a href="http://localhost:8000"><img src="<?php echo $home . "/assets/images/logo.png" ?>" alt="OSAN"></a>
            </div>
            <div class="col-lg-1 offset-lg-1">
                <ul class="footer-links">
                    <li class="link-item"><a href="<?= $home_url ?>/a-osan">a osan</a></li>
                    <li class="link-item"><a href="#">planos</a></li>
                    <li class="link-item"><a href="<?= $home_url ?>/servicos">serviços</a></li>
                    <li class="link-item"><a href="<?= $home_url ?>/duvidas">dúvidas</a></li>
                    <li class="link-item"><a href="<?= $home_url ?>/parceiros">parceiros</a></li>
                </ul>
            </div>
            <div class="col-lg-2 footer-links">
                <ul class="footer-links">
                    <li class="link-item"><a href="<?= $home_url ?>/depoimentos">depoimentos</a></li>
                    <li class="link-item"><a href="<?= $home_url ?>/blog">notícias</a></li>
                    <li class="link-item"><a href="<?= $home_url ?>/unidades">unidades</a></li>
                    <li class="link-item"><a href="<?= $home_url ?>/contatos">contatos</a></li>
                    <li class="link-item"><a target="_blank" href="https://www.catho.com.br/empregos/osan">trabalhe conosco</a></li>
                </ul>
            </div>
            <div class="col-lg-4 footer-tel">

            <!-- REFATORANDO O CÓDIGO -->
                <button type="button" class="btn btn-blue">
                    <small>Em caso de falecimento ligue<br><span>0800-017 8000</span></small>
                </button>

                <button type="button" class="btn btn-whats">
                        <small class="small-text">Fale conosco via WhatsApp</small><br><span class="telefone">(13) 99768870</span>
                </button>

                <button type="button" class="btn btn-call">
                    <small class="small-text">Atendimento</small><br><span class="telefone">(13) 3228-8000</span>
                </button>

                <!-- REFATORANDO O CÓDIGO -->

            </div>
            <div class="col-lg-2 footer-network">
                <p class="text-center">Acompanhe nas Redes Sociais</p>
                <div class="row mt-4">
                    <div class="col-lg-12 d-flex justify-content-between">
                        <i class="fab fa-facebook-f" aria-hidden="true"></i>
                        <i class="fab fa-twitter" aria-hidden="true"></i>
                        <i class="fab fa-linkedin-in" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-lg-12">
                        <small class="developed-by">Desenvolvido por <a href="www.kbrtec.com.br">KBRTEC</a></small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<!-- FOOTER DESKTOP -->


<!-- FOOTER MOBILE -->
<div class="d-lg-none">

    <div class="container-fluid border-top">
            <div class="row">
                <div class="col-12">
                    <p>Acesso rápido</p>
                </div>
            </div>

            <div class="row">
                <div class="col-6 footer-links">
                    <ul>
                        <li class="text-uppercase"><a href="<?= $home_url ?>/a-osan">a osan</a></li>
                        <li class="text-uppercase"><a href="#">planos</a></li>
                        <li class="text-uppercase"><a href="<?= $home_url ?>/servicos">serviços</a></li>
                        <li class="text-uppercase"><a href="<?= $home_url ?>/duvidas">dúvidas</a></li>
                        <li class="text-uppercase"><a href="<?= $home_url ?>/parceiros">parceiros</a></li>
                    </ul>
                </div>
                <div class="col-6 footer-links">
                    <ul>
                        <li class="text-uppercase"><a href="<?= $home_url ?>/depoimentos">depoimentos</a></li>
                        <li class="text-uppercase"><a href="<?= $home_url ?>/blog">notícias</a></li>
                        <li class="text-uppercase"><a href="<?= $home_url ?>/unidades">unidades</a></li>
                        <li class="text-uppercase"><a href="<?= $home_url ?>/contatos">contatos</a></li>
                        <li class="text-uppercase"><a target="_blank" href="https://www.catho.com.br/empregos/osan">trabalhe conosco</a></li>
                    </ul>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-6">
                    <p>Acompanhe nas Redes Sociais</p>
                </div>
                <div class="col-2">
                <i class="fab text-osan fa-facebook-f mr-5" aria-hidden="true"></i>
                </div>

                <div class="col-2">
                <i class="fab text-osan fa-twitter" aria-hidden="true"></i>
                </div>

                <div class="col-2">
                <i class="fab text-osan fa-linkedin-in" aria-hidden="true"></i>
                </div>
            </div>
            
            <div class="row pt-2 pb-5 border-top">
                <div class="col-3">

                </div>
                <div class="col-6">
                    <small class="text-center">Desenvolvido por KBRTEC</small>
                </div>
                <div class="col-3">

                </div>
            </div>
<!-- FOOTER MOBILE -->

<?php wp_footer(); ?>
        </body>
    </head>
</html>