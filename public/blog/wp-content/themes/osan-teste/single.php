<?php get_header(); ?>


<section>
    <div class="container-fluid background-noticias">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <small class="caminho">Você está em: Home /<span class="text-info"> Notícias</span></small>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <p class="titulo">Notícias</p>
                        <div class="w-100 mt-5"></div>
                    </div>
                    <div class="col-lg-5">
                        <div class="searchbar">
                            <input class="search_input" type="text" name="">
                            <a href="#" class="search_icon"><i class="fas fa-search" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="container noticias-box">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                     <div class="article">
                         <h1 class="article-title"><?php wp_title(false); ?></h1>
                <div>
                    <?php the_post_thumbnail('post-thumbnail', array('class' => 'article-img')); ?>
                </div>

                <div class="article-content">
                    <?php the_content(); ?>
                </div>  
            </div>
                            
                                 
        <?php endwhile;
                    else : ?>
        <?php get_404_template(); ?>
    <?php endif; ?>

        </div>


        <div class="container post-read-too">
            <p class="read-too">Leia Também</p>
        </div>

        <div class="container">
            <div class="card-deck single-deck">


                <?php $the_query = new WP_Query('posts_per_page=4'); ?>
                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                    <div class="card card-shadow">
                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', array('class' => 'img-fluid')); ?></a>
                        <div class="card-body">
                            <a class="card-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                    </div>

                <?php endwhile; ?>

            </div>
        </div>
        <?php get_footer(); ?>