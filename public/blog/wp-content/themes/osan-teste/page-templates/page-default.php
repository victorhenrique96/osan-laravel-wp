<?php
/**
 * Template Name: Pagina Novidá
 *
 * @package WordPress
 * @subpackage Novidá
 * @since 1.0
 */
 
get_header(); ?>
 
<div class="container">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
 
            <?php
            while ( have_posts() ) : the_post();
 
                the_content();
 
            endwhile;
            ?>
        </main>
    </div>
</div>
 
<?php get_footer();