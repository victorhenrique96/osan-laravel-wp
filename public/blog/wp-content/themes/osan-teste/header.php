<!DOCTYPE html>
<html lang="pt-BR">

<head>
	<?php $home = get_template_directory_uri(); ?>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>OSAN</title>
	<!-- Menu WordPress -->
	<?php wp_head(); ?>
</head>
<?php $home_url = "http://ambiente-desenvolvimento.provisorio.ws/Victor/osan" ?>
<body>

<header>
            <div class="d-none d-lg-block">

<!-- HEADER -->
<div class="container">
        <div class="row pt-3 pb-3">
            <div class="col-lg-2">
                <a href="<?= $home_url ?>"><img src="<?php echo $home . "/assets/images/logo.png" ?>" alt="OSAN"></a>
            </div>
            <div class="col-lg-1">

            </div>
            <div class="col-lg-3">
                <button type="button" class="btn button-blue d-flex">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="d-block text-btn">Em caso de<br> falecimento ligue</div><span class="d-block">0800-017 8000</span>
                        </div>
                    </div>
                </button>
            </div>
            <div class="col-lg-3">
                <button type="button" class="btn button-light-blue d-flex">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="d-block text-btn">Clique aqui para <br> falar</div><span class="d-block">Por WhatsApp</span>
                        </div>
                    </div>
                </button>
            </div>
            <div class="col-lg-2 mt-2 d-flex">
                <div class="row d-flex">
                    <div class="col-lg-12">
                        <a class="area-cliente" href="#">
                            <i class="fas fa-user" aria-hidden="true"></i>
                            <small class="text-uppercase">Área do cliente</small>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
<!-- HEADER -->



<!-- MENU HEADER -->
<div class="container-fluid border-top border-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mt-1">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

                                <li class="nav-item">
                                    <a class="nav-link" href="<?= $home_url ?>/a-osan">A Osan</a>
                                </li>

                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="false" aria-expanded="true">
                                        Planos
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="<?= $home_url ?>/plano-classico">Plano Clássico</a>
                                        <a class="dropdown-item" href="<?= $home_url ?>/plano-empresarial">Plano Empresarial</a>
                                    </div>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="<?= $home_url ?>/servicos">Serviços</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="<?= $home_url ?>/duvidas">Dúvidas</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="<?= $home_url ?>/parceiros">Parceiros</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="<?= $home_url ?>/depoimentos">Depoimentos</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="<?= $home_url ?>/blog">Notícias</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="<?= $home_url ?>/unidades">Unidades</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="<?= $home_url ?>/contatos">Contatos</a>
                                </li>

                                <li class="nav-item nav-icon">
                                    <a class="nav-link" href="fb.com"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="twitter.com"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                                </li>

                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MENU HEADER -->


<!-- HEADER MOBILE -->
<div class="d-lg-none">

    <div class="row border-bottom">
        <div class="col-6 no-padmag">
            <button type="button" class="btn button-blue">
                <small>Em caso de falecimento ligue<br><span>0800-017 8000</span></small>
            </button>
        </div>
        <div class="col-6 no-padmag">
            <button type="button" class="btn button-light-blue">
                <small>Clique aqui para falar<br><span>Por WhatsApp</span></small>
            </button>
        </div>
    </div>

    <div class="row mt-2 pb-2 border-bottom">
        <div class="col-4 d-flex justify-content-around no-padmag">
            <i class="fab fa-facebook-f" aria-hidden="true"></i>
        </div>
        <div class="col-4 d-flex justify-content-around no-padmag">
            <i class="fab fa-twitter" aria-hidden="true"></i>
        </div>
        <div class="col-4 d-flex justify-content-around no-padmag">
            <i class="fab fa-linkedin-in" aria-hidden="true"></i>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-12 d-flex justify-content-center no-padmag">
            <a href="#"><img class="logo-osan" src="<?php echo $home . "/assets/images/logo.png" ?>" alt="OSAN"></a>
        </div>
    </div>

    <div class="row mt-2 mb-2">
        <div class="col-12 no-padmag">
            <nav class="navbar navbar-expand-lg navbar-light">

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span><img src="<?php echo $home . "/assets/images/icon-menu-mobile.png" ?>">MENU</span>
                </button>


                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav d-flex">
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="#">A Osan</a>
                        </li>

                        <li class="nav-item justify-content-center dropdown">
                            <div class="dropdown">
                                <a class="btn nav-link dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Planos</a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                    <a class="dropdown-item" href="<?= $home_url ?>">Plano Clássico</a>
                                    <a class="dropdown-item" href="<?= $home_url ?>">Plano Empresarial</a>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="<?= $home_url ?>/servicos">Serviços</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="<?= $home_url ?>/duvidas">Dúvidas</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="<?= $home_url ?>/parceiros">Parceiros</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="<?= $home_url ?>/depoimentos">Depoimentos</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="<?= $home_url ?>/blog">Notícias</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="<?= $home_url ?>/unidades">Unidades</a>
                        </li>
                        <li class="nav-item  justify-content-center">
                            <a class="nav-link" href="<?= $home_url ?>/contatos">Contatos</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="<?= $home_url ?>"><span><img class="pr-2" src="<?php echo $home . "/assets/images/user.png" ?>"></span>Área do cliente</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>
<!-- HEADER MOBILE -->
