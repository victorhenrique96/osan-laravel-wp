@extends('layouts.app')

@section('title', 'Perguntas frequentes')

@section('content')
<div class="container-fluid background-duvidas">
    <section>
        @include('layouts.breadcrumb-default')
    </section>

    <div class="container painel-faq">
        <div class="panel-group" id="faqAccordion">
            <div class="panel">
                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question0">
                    <h4 class="panel-title">O que fazer em caso de falecimento?<i class="float-right icone-fechado pr-2 fas fa-chevron-down"></i><i class="float-right icone-aberto pr-2 fas fa-chevron-up"></i></h4>
                </div>
                <div id="question0" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusamus voluptatem saepe consectetur corporis atque magnam sit nesciunt voluptate nam. Autem!</p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question1">
                    <h4 class="panel-title">O que fazer caso o falecimento tenha ocorrido em casa?<i class="float-right icone-fechado pr-2 fas fa-chevron-down"></i><i class="float-right icone-aberto pr-2 fas fa-chevron-up"></i></h4>
                </div>
                <div id="question1" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusamus voluptatem saepe consectetur corporis atque magnam sit nesciunt voluptate nam. Autem!</p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question2">
                    <h4 class="panel-title">Como lidar com o luto de um colega de trabalho?<i class="float-right pr-2 icone-fechado fas fa-chevron-down"></i><i class="float-right icone-aberto pr-2 fas fa-chevron-up"></i></h4>
                </div>
                <div id="question2" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>A psicóloga também explica que é comum que o colaborador enlutado mude de comportamentose afaste dos colegas de trabalho, o que pode gerar incompreensão e críticas, fazendo com que a pessoa sofra pelo luto e pela exclusão dos colegas.</p>
                    </div>
                </div>
            </div>
            <div class="panel">
                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question3">
                    <h4 class="panel-title">Quais os documentos para emissão da certidão de Óbito?<i class="float-right pr-2 icone-fechado fas fa-chevron-down"></i><i class="float-right icone-aberto pr-2 fas fa-chevron-up"></i></h4>
                </div>
                <div id="question3" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam amet pariatur obcaecati odit ipsam porro.</p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question4">
                    <h4 class="panel-title">Preciso pagar taxas de Cemitério?<i class="float-right pr-2 icone-fechado fas fa-chevron-down"></i><i class="float-right icone-aberto pr-2 fas fa-chevron-up"></i></h4>
                </div>
                <div id="question4" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam amet pariatur obcaecati odit ipsam porro.</p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question5">
                    <h4 class="panel-title">Como funciona uma cremação?<i class="float-right pr-2 icone-fechado  fas fa-chevron-down"></i><i class="float-right icone-aberto pr-2 fas fa-chevron-up"></i></h4>
                </div>
                <div id="question5" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam amet pariatur obcaecati odit ipsam porro.</p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question6">
                    <h4 class="panel-title">Roteiro passo a passo para o velório, sepultamento ou cremação.<i class="float-right icone-fechado  pr-2 fas fa-chevron-down"></i><i class="float-right icone-aberto pr-2 fas fa-chevron-up"></i></h4>
                </div>
                <div id="question6" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam amet pariatur obcaecati odit ipsam porro.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<section>
    @include('layouts.pagination')
</section>
@endsection