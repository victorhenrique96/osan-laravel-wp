@extends('layouts.app')

@section('title', 'Area do Cliente')

@section('content')
<div class="container-fluid background-servicos">
    <section>
        @include('layouts.breadcrumb-default')
    </section>

    <div class="container servicos-box-alt">
        <div class="row">
            <div class="col-lg-8">
                <div class="row">
                    <p class="text-osan titulo">Contratos</p>
                </div>
                <div class="row">
                    Confiram os contratos dos serviços oferecidos pela OSAN.
                </div>
                <div class="row m-2 mt-5">
                    <div class="col-lg-3">
                        <img class="mt-2" src="{{asset('/images/document.png')}}" alt="...">
                        <p>Contrato de número <b>0001</b> atualizado em 2018</p>
                    </div>
                    <div class="col-lg-3">
                        <img class="mt-2" src="{{asset('/images/document.png')}}" alt="...">
                        <p>Contrato de número <b>0001</b> atualizado em 2018</p>
                    </div>
                    <div class="col-lg-3">
                        <img class="mt-2" src="{{asset('/images/document.png')}}" alt="...">
                        <p>Contrato de número <b>0001</b> atualizado em 2018</p>
                    </div>
                    <div class="col-lg-3">
                        <img class="mt-2" src="{{asset('/images/document.png')}}" alt="...">
                        <p>Contrato de número <b>0001</b> atualizado em 2018</p>
                    </div>
                </div>
                <div class="row m-2">
                    <div class="col-lg-3">
                        <img class="mt-2" src="{{asset('/images/document.png')}}" alt="...">
                        <p>Contrato de número <b>0001</b> atualizado em 2018</p>
                    </div>
                    <div class="col-lg-3">
                        <img class="mt-2" src="{{asset('/images/document.png')}}" alt="...">
                        <p>Contrato de número <b>0001</b> atualizado em 2018</p>
                    </div>
                    <div class="col-lg-3">
                        <img class="mt-2" src="{{asset('/images/document.png')}}" alt="...">
                        <p>Contrato de número <b>0001</b> atualizado em 2018</p>
                    </div>
                    <div class="col-lg-3">
                        <img class="mt-2" src="{{asset('/images/document.png')}}" alt="...">
                        <p>Contrato de número <b>0001</b> atualizado em 2018</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="row">
                <p class="text-osan titulo">Segunda via do boleto</p>
                </div>
                <div class="row">
                    Caso seja necessário, gere a 2º via de seus boletos.
                </div>
                <div class="row">
                    <div class="w-100 mt-2 mb-2"></div>
                    <input type="button" class="btn button-blue pt-3 pb-3 rounded-pill" value="Acessar boletos">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection