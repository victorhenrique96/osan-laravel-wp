<!-- HEADER MOBILE -->
<div class="d-lg-none">

    <div class="row border-bottom">
        <div class="col-6 no-padmag">
            <button type="button" class="btn button-blue">
                <small>Em caso de falecimento ligue<br><span>0800-017 8000</span></small>
            </button>
        </div>
        <div class="col-6 no-padmag">
            <button type="button" class="btn button-light-blue">
                <small>Clique aqui para falar<br><span>Por WhatsApp</span></small>
            </button>
        </div>
    </div>

    <div class="row mt-2 pb-2 border-bottom">
        <div class="col-4 d-flex justify-content-around no-padmag">
            <i class="fab fa-facebook-f"></i>
        </div>
        <div class="col-4 d-flex justify-content-around no-padmag">
            <i class="fab fa-twitter"></i>
        </div>
        <div class="col-4 d-flex justify-content-around no-padmag">
            <i class="fab fa-linkedin-in"></i>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-12 d-flex justify-content-center no-padmag">
            <a href="{{url('/')}}"><img class="logo-osan" src="{{asset('/images/logo.png')}}" alt="OSAN"></a>
        </div>
    </div>

    <div class="row mt-2 mb-2">
        <div class="col-12 no-padmag">
            <nav class="navbar navbar-expand-lg navbar-light">

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span><img src="{{asset('images/icon-menu-mobile.png')}}">MENU</span>
                </button>


                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav d-flex">
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="{{url('a-osan')}}">A Osan</a>
                        </li>

                        <li class="nav-item justify-content-center dropdown">
                            <div class="dropdown">
                                <a class="btn nav-link dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Planos</a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                    <a class="dropdown-item" href="{{url('plano-classico')}}">Plano Clássico</a>
                                    <a class="dropdown-item" href="{{url('plano-empresarial')}}">Plano Empresarial</a>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="{{url('/servicos')}}">Serviços</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="{{url('/duvidas')}}">Dúvidas</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="{{url('/parceiros')}}">Parceiros</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="{{url('/depoimentos')}}">Depoimentos</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="{{url('/blog')}}">Notícias</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="{{url('/unidades')}}">Unidades</a>
                        </li>
                        <li class="nav-item  justify-content-center">
                            <a class="nav-link" href="{{url('/contatos')}}">Contatos</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="{{url('area-cliente')}}"><span><img class="pr-2" src="{{asset('/images/user.png')}}"></span>Área do cliente</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>
<!-- HEADER MOBILE -->

