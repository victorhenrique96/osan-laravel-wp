<!-- HEADER -->
<div class="container">
    <div class="row header-style">
        <div class="col-lg-2">
            <a href="{{url('/')}}"><img src="{{asset('/images/logo.png')}}" alt="OSAN"></a>
        </div>
        <div class="col-lg-4 header-buttons">
            <button type="button" class="button-blue">
                <span class="text-btn">Em caso de falecimento ligue
                    <span class="number-btn">
                        <br>0800-017 8000</span>
                </span>
            </button>

            <button type="button" class="d-none button-light-blue">
                <span class="text-btn">Clique aqui para falar
                    <span class="number-btn">
                        <br>Por WhatsApp</span>
                </span>
            </button>

        </div>
        <div class="col-lg-4">
            <div class="row d-flex">
                <a class="area-cliente" href="{{url('/area-cliente')}}">
                    <i class="fas fa-user"></i>
                    <small class="text-uppercase">Área do cliente</small>
                </a>
                {{-- <div class="container d-flex justify-content-between">
                    <a href="{{url('/')}}"><img src="{{asset('images/lang/br.png')}}" alt="Versão em Português (Brasil)"></a>
                    <a href="{{url('/en')}}"><img src="{{asset('images/lang/usa.png')}}" alt="Versão em Inglês"></a>
                </div> --}}
            </div>
        </div>
    </div>
</div>
<!-- HEADER -->

<!-- MENU HEADER -->
<div class="container-fluid border-top border-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 mt-1">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

                            <li class="nav-item">
                                <a class="nav-link" href="{{url('a-osan')}}">A Osan</a>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="false" aria-expanded="true">
                                    Planos
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="{{url('plano-classico')}}">Plano Clássico</a>
                                    <a class="dropdown-item" href="{{url('plano-empresarial')}}">Plano Empresarial</a>
                                </div>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/servicos')}}">Serviços</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/duvidas')}}">Dúvidas</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/parceiros')}}">Parceiros</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/depoimentos')}}">Depoimentos</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/blog')}}">Notícias</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/unidades')}}">Unidades</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/contatos')}}">Contatos</a>
                            </li>

                            <li class="nav-item nav-icon">
                                <a class="nav-link" href="fb.com"><i class="fab fa-facebook-f"></i></a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="twitter.com"><i class="fab fa-twitter"></i></a>
                            </li>

                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
</div>
<!-- MENU HEADER -->