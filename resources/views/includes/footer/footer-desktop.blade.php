<div class="d-none d-lg-block">

    <div class="container-fluid border-top">
        <div class="row">
        <div class="w-100 mt-3"></div>
            <div class="col-lg-4">
            </div>
            <div class="col-lg">
                <p>Acesso rápido</p>
            </div>
        </div>
<div class="container">
    <div class="row">
            <div class="col-lg-2">
                <a href="{{url('/')}}"><img src="{{asset('/images/logo.png')}}" alt="OSAN"></a>
            </div>
            <div class="col-lg-1 offset-lg-1">
                <ul class="footer-links">
                    <li class="text-uppercase"><a href="{{url('/a-osan')}}">a osan</a></li>
                    <li class="text-uppercase"><a href="#">planos</a></li>
                    <li class="text-uppercase"><a href="{{url('/servicos')}}">serviços</a></li>
                    <li class="text-uppercase"><a href="{{url('/duvidas')}}">dúvidas</a></li>
                    <li class="text-uppercase"><a href="{{url('/parceiros')}}">parceiros</a></li>
                </ul>
            </div>
            <div class="col-lg-2 footer-links">
                <ul class="footer-links">
                    <li class="text-uppercase"><a href="{{url('/depoimentos')}}">depoimentos</a></li>
                    <li class="text-uppercase"><a href="{{url('/blog')}}">notícias</a></li>
                    <li class="text-uppercase"><a href="{{url('/unidades')}}">unidades</a></li>
                    <li class="text-uppercase"><a href="{{url('/contatos')}}">contatos</a></li>
                    <li class="text-uppercase"><a target="_blank" href="https://www.catho.com.br/empregos/osan">trabalhe conosco</a></li>
                </ul>
            </div>
            <div class="col-lg-4 footer-tel">
                <!-- REFATORANDO O CÓDIGO -->
                <button type="button" class="btn btn-blue">
                    <small>Em caso de falecimento ligue<br><span>0800-017 8000</span></small>
                </button>

                <button type="button" class="btn btn-whats">
                        <small class="small-text">Fale conosco via WhatsApp</small><br><span class="telefone">(13) 99768870</span>
                </button>

                <button type="button" class="btn btn-call">
                    <small class="small-text">Atendimento</small><br><span class="telefone">(13) 3228-8000</span>
                </button>

                <!-- REFATORANDO O CÓDIGO -->
            </div>
            <div class="col-lg-2 footer-network">
                <p class="text-center">Acompanhe nas Redes Sociais</p>
                <div class="row mt-4">
                    <div class="col-lg-12 d-flex justify-content-between">
                        <i class="fab text-osan fa-facebook-f"></i>
                        <i class="fab text-osan fa-twitter"></i>
                        <i class="fab text-osan fa-linkedin-in"></i>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-lg-12">
                        <small>Desenvolvido por <a href="www.kbrtec.com.br">KBRTEC</a></small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>