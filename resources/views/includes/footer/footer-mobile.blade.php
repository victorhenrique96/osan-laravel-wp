
<!-- FOOTER MOBILE -->
<div class="d-lg-none">

    <div class="container-fluid border-top">
            <div class="row">
                <div class="col-12">
                    <p>Acesso rápido</p>
                </div>
            </div>

            <div class="row">
                <div class="col-6 footer-links">
                    <ul>
                        <li class="text-uppercase"><a href="{{url('/a-osan')}}">a osan</a></li>
                        <li class="text-uppercase"><a href="#">planos</a></li>
                        <li class="text-uppercase"><a href="{{url('/servicos')}}">serviços</a></li>
                        <li class="text-uppercase"><a href="{{url('/duvidas')}}">dúvidas</a></li>
                        <li class="text-uppercase"><a href="{{url('/parceiros')}}">parceiros</a></li>
                    </ul>
                </div>
                <div class="col-6 footer-links">
                    <ul>
                        <li class="text-uppercase"><a href="{{url('/depoimentos')}}">depoimentos</a></li>
                        <li class="text-uppercase"><a href="{{url('/blog')}}">notícias</a></li>
                        <li class="text-uppercase"><a href="{{url('/unidades')}}">unidades</a></li>
                        <li class="text-uppercase"><a href="{{url('/contatos')}}">contatos</a></li>
                        <li class="text-uppercase"><a target="_blank" href="https://www.catho.com.br/empregos/osan">trabalhe conosco</a></li>
                    </ul>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-6">
                    <p>Acompanhe nas Redes Sociais</p>
                </div>
                <div class="col-2">
                <i class="fab text-osan fa-facebook-f mr-5"></i>
                </div>

                <div class="col-2">
                <i class="fab text-osan fa-twitter"></i>
                </div>

                <div class="col-2">
                <i class="fab text-osan fa-linkedin-in"></i>
                </div>
            </div>
            
            <div class="row pt-2 pb-5 border-top">
                <div class="col-3">

                </div>
                <div class="col-6">
                    <small class="text-center">Desenvolvido por KBRTEC</small>
                </div>
                <div class="col-3">

                </div>
            </div>
<!-- FOOTER MOBILE -->