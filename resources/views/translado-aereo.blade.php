@extends('layouts.app')

@section('title', 'Translado Aéreo')

@section('content')
<div class="container-fluid background-servicos">
    <section>
        @include('layouts.breadcrumb-default')
    </section>

<div class="container servicos-box-alt">
    <div class="row mt-3">
    <div class="col-lg-6">
    <img class="translado-aereo" src="{{asset('/images/servicos/translado-aereo.png')}}">
    </div>
        <div class="col-lg-6">
            <p class="h2 pb-5">Translado aéreo</p>
                <div class="w-100">
                </div>
            <p>Com parceria com várias companhias aéreas no brasil, conseguimos executar o transporte aéreo com todos os serviços funerários inclusos. Somos uma das poucas empresas a ter o melhor preço de transporte funerário no Brasil.</p>
            <div class="w-100">
            </div>
            <p>Com essa parceria, além da comodidade de ter todos os serviços funerários inclusos, conseguimos um preço padrão para todos os serviços. Somos a empresa líder no transporte de corpos em avião comercial.</p>
            <div class="w-100 mt-5 mb-5">
            </div>
            <button type="button" class="btn btn-success w-100">Solicite um orçamento agora mesmo!</button>
        </div>
    </div>


</div>

@endsection