@extends('layouts.app')

@section('title', 'Parceiros')

@section('content')
<div class="container-fluid background-servicos">
<section>
        @include('layouts.breadcrumb-default')
    </section>

    <div class="container parceiros-box">
        <div class="container servicos parceiros-page">
<!-- PRIMEIRA LINHA -->
        <div class="card-group text-center">
        <div class="card">
            <div class="card-body">
                    <div class="row">
                        <div class="col-lg-4 border-right">
                            <img class="card-img-top w-50 m-5" src="{{asset('/images/parceiros/bakery.png')}}" alt="Bakery Company">
                        </div>
                        <div class="col-lg-8 mt-2">
                            <h2 class="text-osan">Bakery Company</h5>
                            <div class="w-100"></div>
                            <p class="text-secondary mt-5 font-weight-light">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nobis culpa magnam cum porro soluta laudantium perferendis dolorum saepe? Repudiandae aut perspiciatis molestias quidem, esse voluptas quod tenetur natus odio doloremque tempore enim sint est minus vel itaque eveniet iure dolor eligendi! Omnis minus voluptates fugit vitae, eaque iste sit. Quas! Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt, quos vel. Dolor natus id magnam, facere incidunt fugit.</p><div class="w-100"></div><p class="text-secondary mt-5 font-weight-light">Eveniet, dolores animi. Et earum rerum quaerat error cum vel ea maiores velit. Aliquam qui accusamus dolorum rerum? Impedit consectetur asperiores tempora eum at magni sit fugiat aperiam quasi, enim facere nemo, unde quod placeat accusantium sequi a doloribus voluptate, sed sapiente!</p>
                            <div class="w-100 mt-5"></div>
                        </div>
                    </div>
            </div>
        </div>
<!-- PRIMEIRA LINHA -->
        </div>
    </div>
</div>


<div class="container veja-parceiros mt-5">
    <p class="text-osan">Veja outros parceiros</p>
    <div class="w-100"></div>
        <div class="row">
            <div class="col-lg-12">
            <div class="card-group">
                <div class="card text-center">
                    <div class="card-body">
                    <img class="card-img-top w-50" src="{{asset('/images/parceiros/beer.png')}}" alt="Beer Coffe">
                    <h5 class="card-title"><a href="{{url('/parceiros')}}">Beer Coffe</a></h5>
                    </div>
                </div>
                <div class="card text-center">
                    <div class="card-body">
                        <img class="card-img-top w-50" src="{{asset('/images/parceiros/company.png')}}" alt="Company">
                        <h5 class="card-title"><a href="{{url('/parceiros')}}">Company</a></h5>
                    </div>
                </div>
                <div class="card text-center">
                    <div class="card-body">
                        <img class="card-img-top w-50" src="{{asset('/images/parceiros/ousada.png')}}" alt="Ousada Flor">
                        <h5 class="card-title"><a href="{{url('/parceiros')}}">Ousada Flor</a></h5>
                    </div>
                </div>
                <div class="card text-center">
                    <div class="card-body">
                        <img class="card-img-top w-50" src="{{asset('/images/parceiros/mult-flores.png')}}" alt="Mult-flores">
                        <h5 class="card-title"><a href="{{url('/parceiros')}}">Mult Flores</a></h5>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="w-100 mt-5"></div>


@endsection