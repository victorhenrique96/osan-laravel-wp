@extends('layouts.app')

@section('title', 'Parceiros')

@section('content')
<div class="container-fluid background-servicos">
    <section>
        @include('layouts.breadcrumb-with-searchbar')
    </section>

    <div class="container parceiros-box">
        <div class="container servicos parceiros-page">
            <!-- PRIMEIRA LINHA -->
            <div class="card-group text-center">
                <div class="card">
                    <div class="card-body">
                        <img class="card-img-top" src="{{asset('/images/parceiros/company.png')}}" alt="Company">
                        <h5 class="card-title">Company</h5>
                        <h5 class="card-button"><button class="btn rounded-pill pb-1 pt-1 pl-2 pr-2 btn-outline-secondary text-uppercase">mais informações</button></h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <img class="card-img-top" src="{{asset('/images/parceiros/autumn.png')}}" alt="Autumn">
                        <h5 class="card-title">Autumn Leares</h5>
                        <h5 class="card-button"><button class="btn rounded-pill pb-1 pt-1 pl-2 pr-2 btn-outline-secondary text-uppercase">mais informações</button></h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <img class="card-img-top" src="{{asset('/images/parceiros/fryn.png')}}" alt="Fryn Jewelry">
                        <h5 class="card-title">Fryn Jewelry</h5>
                        <h5 class="card-button"><button class="btn rounded-pill pb-1 pt-1 pl-2 pr-2 btn-outline-secondary text-uppercase">mais informações</button></h5>
                    </div>
                </div>
            </div>
            <!-- PRIMEIRA LINHA -->

            <!-- SEGUNDA LINHA -->
            <div class="card-group text-center">
                <div class="card">
                    <div class="card-body">
                        <img class="card-img-top" src="{{asset('/images/parceiros/beer.png')}}" alt="Beer Coffe">
                        <h5 class="card-title">Beer Coffe</h5>
                        <h5 class="card-button"><button class="btn rounded-pill pb-1 pt-1 pl-2 pr-2 btn-outline-secondary text-uppercase">mais informações</button></h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <a href="{{url('/parceiros/bakery')}}">
                            <img class="card-img-top" src="{{asset('/images/parceiros/bakery.png')}}" alt="Bakery Company">
                            <h5 class="card-title">Bakery Company</h5>
                            <h5 class="card-button"><button class="btn rounded-pill pb-1 pt-1 pl-2 pr-2 btn-outline-secondary text-uppercase">mais informações</button></h5>
                        </a>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <img class="card-img-top" src="{{asset('/images/parceiros/ousada.png')}}" alt="Ousada Flor">
                        <h5 class="card-title">Ousada Flor</h5>
                        <h5 class="card-button"><button class="btn rounded-pill pb-1 pt-1 pl-2 pr-2 btn-outline-secondary text-uppercase">mais informações</button></h5>
                    </div>
                </div>
            </div>
            <!-- SEGUNDA LINHA -->

            <!-- TERCEIRA LINHA -->
            <div class="card-group text-center">
                <div class="card">
                    <div class="card-body">
                        <img class="card-img-top" src="{{asset('/images/parceiros/mult-flores.png')}}" alt="Mult-flores">
                        <h5 class="card-title">Mult Flores</h5>
                        <h5 class="card-button"><button class="btn rounded-pill pb-1 pt-1 pl-2 pr-2 btn-outline-secondary text-uppercase">mais informações</button></h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <img class="card-img-top" src="{{asset('/images/parceiros/garden-sul.png')}}" alt="Gardeh Sul">
                        <h5 class="card-title">Gardeh Sul</h5>
                        <h5 class="card-button"><button class="btn rounded-pill pb-1 pt-1 pl-2 pr-2 btn-outline-secondary text-uppercase">mais informações</button></h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <img class="card-img-top" src="{{asset('/images/parceiros/faca-bonito.png')}}" alt="Faça Bonito">
                        <h5 class="card-title">Faça Bonito</h5>
                        <h5 class="card-button"><button type="button" class="btn rounded-pill pb-1 pt-1 pl-2 pr-2 btn-outline-secondary text-uppercase">mais informações</button></h5>
                    </div>
                </div>
            </div>
            <!-- TERCEIRA LINHA -->
        </div>
    </div>
</div>

<section>
    @include('layouts.pagination')
</section>

@endsection