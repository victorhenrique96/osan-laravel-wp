@extends('layouts.app')

@section('title', 'Notícias')

@section('content')
<div class="container-fluid">
    <section>
        @include('layouts.breadcrumb-default')
    </section>
</div>

<div class="container parceiros-box">
    <h2 class="text-osan text-justify">Lidando com o luto de um colega de trabalho</h2>
    <div class="w-100 p-2"></div>
    <img class="img-fluid" src="{{asset('/images/noticias/img-noticia.png')}}">
    <div class="w-100 p-2"></div>
    <span class="mt-2 text-secondary text-justify">Lidar com o luto de outra pessoa costuma ser bastante complicado, independente de quem seja. Para as empresas, é particularmente delicado lidar com o luto de um colega de trabalho, pois a queda de produtividade e socialização costumam diminuir bastante neste período.</span>
    <div class="w-100 p-2"></div>
    <span class="text-secondary text-justify">Segundo Mariana Simonetti, psicóloga do Grupo Vila, o ideal seria que todas as empresas oferecessem um suporte psíquico ao colaborador, ou até mesmo um espaço de escuta. “Pequenas ações como um momento de conscientização com os funcionários de forma geral já é o suficiente para contribuir com a saúde e bem-estar deles”, afirma.A psicóloga também explica que é comum que o colaborador enlutado mude de comportamento e se afaste dos colegas de trabalho, o que pode gerar incompreensão e críticas, fazendo com que a pessoa sofra pelo luto e pela exclusão dos colegas.</span>
    <div class="w-100 p-2"></div>
    <span class="text-secondary text-justify">“Por isso a necessidade de um acompanhamento e conscientização psicológica, para que não só a pessoa enlutada saiba lidar com os seus sentimentos, mas também todas as pessoas que estiverem ao redor dela”, completa.
     Dando essa abertura e construindo esse ambiente saudável, o enlutado se sente mais tranquilo. Mas Mariana alerta: oferecer ajuda é diferente de aceitar qualquer tipo de comportamento com o luto como justificativa. “A assistência deve existir, mas o funcionário não pode se sentir no direito de agir como quiser. É preciso haver compreensão das duas partes”, explica.</span>
</div>

<div class="container veja-noticias mt-5">
    <p class="text-osan">Leia também</p>
    <div class="w-100"></div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card-group">
                <div class="card">
                    <div class="card-body">
                        <img class="card-img-top w-100" src="{{asset('/images/noticias/footer-noticia1.png')}}" alt="Beer Coffe">
                        <h5 class="card-title pt-2 text-osan font-weight-bold text-uppercase">Apoio a família</h5>
                        <span class="card-text text-uppercase">Porque homenagear com flores?</span>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <img class="card-img-top w-100" src="{{asset('/images/noticias/footer-noticia2.png')}}" alt="Company">
                        <h5 class="card-title pt-2 text-osan font-weight-bold text-uppercase">Vida</h5>
                        <span class="card-text text-uppercase">lidando com o luto de um colega de trabalho</span>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <img class="card-img-top w-100" src="{{asset('/images/noticias/footer-noticia3.png')}}" alt="Ousada Flor">
                        <h5 class="card-title pt-2 text-osan font-weight-bold text-uppercase">Familiares</h5>
                        <span class="card-text text-uppercase">diferentes formas de perder o medo da morte</span>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <img class="card-img-top w-100" src="{{asset('/images/noticias/footer-noticia4.png')}}" alt="Mult-flores">
                        <h5 class="card-title pt-2 text-osan font-weight-bold text-uppercase">Dicas</h5>
                        <span class="card-text text-uppercase">porque homenagear com flores?</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection