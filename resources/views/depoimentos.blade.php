@extends('layouts.app')

@section('title', 'Depoimentos')

@section('content')
<div class="container-fluid background-depoimentos">
    <section>
        @include('layouts.breadcrumb-default')
    </section>


    <div class="container parceiros-box">
        <div class="row">
            <div class="col-lg-6">
                <img class="float-left mr-4" src="{{asset('/images/chat-like.png')}}">
                <span class="font-italic text-secondary">Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.
                </span>
                <small class="nome-familia font-weight-bold">Devid e família</small>
            </div>
            <div class="col-lg-6">
                <img class="float-left mr-4" src="{{asset('/images/chat-like.png')}}">
                <span class="font-italic text-secondary">Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.
                </span>
                <small class="nome-familia font-weight-bold">Devid e família</small>
            </div>
            <div class="w-100 pt-2 pb-1"></div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <img class="float-left mr-4" src="{{asset('/images/chat-like.png')}}">
                <span class="font-italic text-secondary">Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.
                </span>
                <small class="nome-familia font-weight-bold">Devid e família</small>
            </div>
            <div class="col-lg-6">
                <img class="float-left mr-4" src="{{asset('/images/chat-like.png')}}">
                <span class="font-italic text-secondary">Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.
                </span>
                <small class="nome-familia font-weight-bold">Devid e família</small>
            </div>
            <div class="w-100 pt-2 pb-1"></div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <img class="float-left mr-4" src="{{asset('/images/chat-like.png')}}">
                <span class="font-italic text-secondary">Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.
                </span>
                <small class="nome-familia font-weight-bold">Devid e família</small>
            </div>
            <div class="col-lg-6">
                <img class="float-left mr-4" src="{{asset('/images/chat-like.png')}}">
                <span class="font-italic text-secondary">Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.
                </span>
                <small class="nome-familia font-weight-bold">Devid e família</small>
            </div>
            <div class="w-100 pt-2 pb-1"></div>
        </div>
    </div>
</div>

<section>
    @include('layouts.pagination')
</section>

@endsection