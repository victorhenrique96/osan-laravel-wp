@extends('layouts.app')

@section('title', 'Plano Empresarial')

@section('content')
<div class="container">
    <div class="row mb-2">
    <div class="col-lg-12">
        <small class="caminho">Você está em: Home /</i><span class="text-info"> @yield('title')</span></small>
    </div>
    </div>
    <div clas="row">
        <div class="col-lg-12">
            <div class="d-flex justify-content-center">
                <img src="{{asset('images/planos/plano-image.png')}}" class="img-fluid img-plano">
                <span class="plano-caption">@yield('title')</span>
                <span class="plano-caption-sub">de Assistência Funeral</span>
                <span class="text-osan h1 font-weight-bold mt-5 text-center d-lg-none d-block">@yield('title')</span>
            </div>
        </div>
    </div>

    <div class="row mt-5 quebra-linha">
        <div class="col-lg-5">
            <p class="text-planos">Para atender ao crescente interesse por parte de empresas privadas, sindicatos e
                associações, a OSAN desenvolveu o <strong>Plano Empresarial de Assistência Funeral</strong>, que tem
                o objetivo de minimizar os transtornos e as responsabilidades quando ocorre um óbito
                de um colaborador ou associado.
            </p>
            <p class="h5 text-osan font-weight-bold w-100"><i class="fas fa-user-friends mt-lg-5 pr-3 mt-3"></i>Dependentes</p>
            <p class="text-planos">O Plano Empresarial de Assistência Funeral permite ao titular incluir como
                dependentes: cônjuge ou companheira (o), pais, sogros, filhos solteiros e outros
                dependentes oficialmente comprovados, desde que sejam autorizados pela entidade.
                Os serviços serão prestados a todo e qualquer usuário, independentemente de faixa
                etária, doenças pré-existentes ou município de residência que fizerem a devida
                adesão.</p>

            <p class="h5 text-osan font-weight-bold w-100"><i class="fas fa-hand-holding-usd mt-lg-5 pr-3 mt-3"></i>Forma de pagamento</p>
            <p class="text-planos">Consulte-nos</p>

            <div class="w-100"></div>

            <p class="h5 text-osan font-weight-bold"><i class="fas fa-phone-square-alt mt-lg-5 pr-3 mt-3"></i>Central de Atendimento 24 horas</p>
            <p class="text-planos">A OSAN disponibiliza aos associados uma Central de Atendimento 24 horas, que
                oferece informações sobre procedimentos funerários, recepciona comunicados de
                falecimento, toma providências referentes ao cerimonial e assessora a família durante
                todo o serviço.</p>
            <div class="w-100"></div>
            <p class="text-planos text-osan font-weight-bold">Em caso de falecimento, ligue gratuitamente para 0800 017 8000.</p>

            <p class="h5 text-osan font-weight-bold"><i class="fas fa-user-tie mt-lg-5 pr-3 mt-3"></i>Departamento do Plano Empresarial de Assistência Funeral</p>
            <p class="text-planos">O Departamento do Plano Empresarial de Assistência Funeral realiza atendimento
                exclusivo para as empresas. Diariamente, são enviados relatórios, por e-mail, contendo
                informações sobre cadastro dos associados.</p>
            <div class="w-100"></div>
            <p class="text-planos">Além disso, é possível esclarecer dúvidas sobre faturas, movimentação de vidas e 2ª
                via de boleto através do telefone (13) 3228 8019 ou e-mail <a href="mailto:planoempresa@osan.com.br">planoempresa@osan.com.br</a></p>
        </div>
        <div class="col-lg-1 no-padmag">
        </div>
        <div clas="col-lg-6">
            <p class="h5 text-osan font-weight-bold w-100 text-cobertura">Cobertura:</p>
            <p class="text-planos-list">
                <ul class="text-list">
                    <li><i class="fas fa-check mr-2"></i>Urna sextavada, envernizada com alça varão e visor;</li>
                    <li><i class="fas fa-check mr-2"></i>Ornamentação;</li>
                    <li><i class="fas fa-check mr-2"></i>Paramentos para velório;</li>
                    <li><i class="fas fa-check mr-2"></i>Livro de Presença;</li>
                    <li><i class="fas fa-check mr-2"></i>Véu para urna;</li>
                    <li><i class="fas fa-check mr-2"></i>Velas próprias para velório;</li>
                    <li><i class="fas fa-check mr-2"></i>Uma Coroa de Flores (1 Bola);</li>
                    <li><i class="fas fa-check mr-2"></i>Higienização do corpo;</li>
                    <li><i class="fas fa-check mr-2"></i>Declaração de óbito;</li>
                    <li><i class="fas fa-check mr-2"></i>Guia de Sepultamento;</li>
                    <li><i class="fas fa-check mr-2"></i>Taxa de Sepultamento em Cemitério Municipal no município da última residência do falecido;</li>
                    <li><i class="fas fa-check mr-2"></i>Agendamento junto ao Cemitério;</li>
                    <li><i class="fas fa-check mr-2"></i>Veículos para remoção e/ou cortejo dentro da abrangência geográfica;</li>
                    <li><i class="fas fa-check mr-2"></i>Veículos para translado estadual e/ou interestadual, até o município de morada da pessoa falecida, com franquia de 400 km percorridos; </li>
                    <li><i class="fas fa-check mr-2"></i>Serviço de café e biscoito nos primeiros momentos do velório, somente nas seguintes regiões: Itanhaém, Mongaguá, Praia Grande, São Vicente, Cubatão, Santos, Guarujá e Bertioga;</li>
                    <li><i class="fas fa-check mr-2"></i>Velório dentro da abrangência geográfica e de acordo com a disponibilidade do dia e hora nos Cemitérios Municipais.</li>
                    <li><i class="fas fa-check mr-2"></i>Velório particular sob denominação Esmeralda na instalação da <b>OSAN</b> em Praia Grande de acordo com a disponibilidade do dia e hora.</li>
                </ul>
            </p>


            <div class="w-100"></div>
            <p class="h5 text-osan font-weight-bold w-100"><i class="fas fa-car mt-lg- pr-3 mt-3"></i>Transporte</p>
            <p class="text-planos">A <strong>OSAN</strong> possui ampla frota com mais de 20 automóveis disponíveis para servir o associado, além de veículos exclusivos para cortejos fúnebres realizados pela empresa na Baixada Santista.</p>
        </div>
    </div>

    <div class="row mt-5 mb-5">
        <div class="col-lg d-flex justify-content-center mb-3 mb-lg-0">
            <button class="rounded-pill button-blue text-uppercase pb-2 pt-2 pl-5 pr-5 border-0 text-white">Contratar este plano</button>
        </div>
        <div class="col-lg d-flex justify-content-center">
            <a href="{{url('/plano-classico')}}">
                <button class="rounded-pill button-blue text-uppercase pb-2 pt-2 pl-5 pr-5 border-0 text-white">Conheça também o plano clássico</button></a>
        </div>
    </div>

</div>
@endsection