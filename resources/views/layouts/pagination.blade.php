<nav class="nav-pages" aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Anterior</a>
            </li>
            <li class="page-item"><a class="page-link text-osan" href="#">1</a></li>
            <li class="page-item"><a class="page-link text-osan" href="#">2</a></li>
            <li class="page-item"><a class="page-link text-osan" href="#">3</a></li>
            <li class="page-item">
                <a class="page-link text-osan" href="#">Próxima</a>
            </li>
        </ul>
    </nav>