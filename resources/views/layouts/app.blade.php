<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') - OSAN</title>
    {{-- Include da Folha de Estilos CSS --}}
    @include('assets.styles.styles')
    {{-- Include da Folha de Estilos CSS --}}
</head>
<body>
        <header>
            @include('layouts.header') 
        </header>
 
        <section>
            @yield('content')
        </section>
 
        <footer>
            @include('layouts.footer')
        </footer>

{{-- Include dos Scripts JS --}}
@include('assets.scripts.scripts')
</body>
</html>