<div class="d-none d-lg-block">

@if(App::getLocale() == 'en')
{{-- <!-- HEADER -->
<div class="container">
        <div class="row pt-3 pb-3">
            <div class="col-lg-2">
                <a href="{{url('/')}}"><img src="{{asset('/images/logo.png')}}" alt="OSAN"></a>
            </div>
            <div class="col-lg-1">

            </div>
            <div class="col-lg-3">
                <button type="button" class="btn button-blue rounded-pill d-flex">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="d-block text-btn">In case of death call us</div><span class="d-block">0800-017 8000</span>
                        </div>
                    </div>
                </button>
            </div>
            <div class="col-lg-3">
                <button type="button" class="btn button-light-blue rounded-pill d-flex">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="d-block text-btn">Click here to send a message</div><span class="d-block">in WhatsApp</span>
                        </div>
                    </div>
                </button>
            </div>
            <div class="col-lg-2 mt-2 d-flex">
                <div class="row d-flex">
                    <div class="col-lg-12">
                        <a class="area-cliente" href="{{url('/area-cliente')}}">
                            <i class="fas fa-user"></i>
                            <small class="text-uppercase">Customer Area</small>
                        </a>
                    </div>
                    <div class="col-lg-12">
                        <div class="container d-flex justify-content-between">
                            <a href="{{url('/')}}"><img src="{{asset('images/lang/br.png')}}" alt="View in Portuguese (Brazil)"></a>
                            <a href="{{url('/en')}}"><img src="{{asset('images/lang/usa.png')}}" alt="View in English (USA)"></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
<!-- HEADER --> --}}
@else

    @include('includes.header.header-desktop')

@endif


{{-- @if(App::getLocale() == 'en')
<!-- MENU HEADER -->
<div class="container-fluid border-top border-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mt-1">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('a-osan')}}">Osan</a>
                                </li>

                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="false" aria-expanded="true">
                                        Plans
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="{{url('plano-classico')}}">Classic Plan</a>
                                        <a class="dropdown-item" href="{{url('plano-empresarial')}}">Business Plan</a>
                                    </div>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/servicos')}}">Services</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/duvidas')}}">FAQ</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/parceiros')}}">Partners</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/depoimentos')}}">Testimonials</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/noticias')}}">News</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/unidades')}}">Units</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/contatos')}}">Contact</a>
                                </li>

                                <li class="nav-item nav-icon">
                                    <a class="nav-link" href="fb.com"><i class="fab fa-facebook-f"></i></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="twitter.com"><i class="fab fa-twitter"></i></a>
                                </li>

                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MENU HEADER -->
@endif --}}


@if(App::getLocale() == 'en')
{{-- <!-- HEADER MOBILE -->
<div class="d-lg-none">

    <div class="row border-bottom">
        <div class="col-6 no-padmag">
            <button type="button" class="btn button-blue">
                <small>Em caso de falecimento ligue<br><span>0800-017 8000</span></small>
            </button>
        </div>
        <div class="col-6 no-padmag">
            <button type="button" class="btn button-light-blue">
                <small>Clique aqui para falar<br><span>Por WhatsApp</span></small>
            </button>
        </div>
    </div>

    <div class="row mt-2 pb-2 border-bottom">
        <div class="col-4 d-flex justify-content-around no-padmag">
            <i class="fab fa-facebook-f"></i>
        </div>
        <div class="col-4 d-flex justify-content-around no-padmag">
            <i class="fab fa-twitter"></i>
        </div>
        <div class="col-4 d-flex justify-content-around no-padmag">
            <i class="fab fa-linkedin-in"></i>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-12 d-flex justify-content-center no-padmag">
            <a href="{{url('/')}}"><img class="logo-osan" src="{{asset('/images/logo.png')}}" alt="OSAN"></a>
        </div>
    </div>

    <div class="row mt-2 mb-2">
        <div class="col-12 no-padmag">
            <nav class="navbar navbar-expand-lg navbar-light">

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span><img src="{{asset('images/icon-menu-mobile.png')}}">MENU</span>
                </button>


                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav d-flex">
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="{{url('a-osan')}}">A Osan</a>
                        </li>

                        <li class="nav-item justify-content-center dropdown">
                            <div class="dropdown">
                                <a class="btn nav-link dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Planos</a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                    <a class="dropdown-item" href="{{url('plano-classico')}}">Plano Clássico</a>
                                    <a class="dropdown-item" href="{{url('plano-empresarial')}}">Plano Empresarial</a>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="{{url('/servicos')}}">Serviços</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="{{url('/duvidas')}}">Dúvidas</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="{{url('/parceiros')}}">Parceiros</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="{{url('/depoimentos')}}">Depoimentos</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="{{url('/blog')}}">Notícias</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="{{url('/unidades')}}">Unidades</a>
                        </li>
                        <li class="nav-item  justify-content-center">
                            <a class="nav-link" href="{{url('/contatos')}}">Contatos</a>
                        </li>
                        <li class="nav-item justify-content-center">
                            <a class="nav-link" href="{{url('area-cliente')}}"><span><img class="pr-2" src="{{asset('/images/user.png')}}"></span>Área do cliente</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>



</div>
<!-- HEADER MOBILE --> --}}
@else

    @include('includes.header.header-mobile')

@endif
